import Utils from './Utils.js';
import Node from './Node.js';

const mat4 = glMatrix.mat4;
const vec3 = glMatrix.vec3;

export default class Player extends Node {

    constructor(options) {
        super(options);
        Utils.init(this, this.constructor.defaults, options);

        this.mousemoveHandler = this.mousemoveHandler.bind(this);
        this.keydownHandler = this.keydownHandler.bind(this);
        this.keyupHandler = this.keyupHandler.bind(this);
        this.keys = {};
    }

    updateProjection() {
        mat4.perspective(this.projection, this.fov, this.aspect, this.near, this.far);
    }
 
    update(keys,dt) {
        const c = this;

        const forward = vec3.set(vec3.create(),
            -Math.sin(c.rotation[1]), 0, -Math.cos(c.rotation[1]));
        const right = vec3.set(vec3.create(),
            Math.cos(c.rotation[1]), 0, -Math.sin(c.rotation[1]));
        const up = vec3.set(vec3.create(),0,2,0);
        const down = vec3.set(vec3.create(),0,-0.01,0);

        // 1: add movement acceleration
        let acc = vec3.create();
        if (keys['KeyA']) {
            vec3.add(acc, acc, forward);
        }
        if (keys['KeyD']) {
            vec3.sub(acc, acc, forward);
        }
        if (keys['KeyW']) {
            vec3.add(acc, acc, right);
        }
        if (keys['KeyS']) {
            vec3.sub(acc, acc, right);
        }
        let vAcc = vec3.create();
        if (c.canJump && keys['Space']){
            c.canJump = false;
            vec3.add(acc,acc,up);
        }
        // 2: update velocity

        vec3.scaleAndAdd(c.velocity, c.velocity, acc, dt * c.acceleration);
        // 3: if no movement, apply friction
        if (!keys['KeyW'] &&
            !keys['KeyS'] &&
            !keys['KeyD'] &&
            !keys['KeyA'])
        {   
            vec3.scale(c.velocity, c.velocity, 1 - c.friction);
        }

        // 4: limit speed
        const len = vec3.len(c.velocity);
        if (len > c.maxSpeed) {
            vec3.scale(c.velocity, c.velocity, c.maxSpeed / len);
        }

    }

    getGlobalTransform() {
        if (!this.parent) {
            return mat4.clone(this.transform);
        } else {
            let transform = this.parent.getGlobalTransform();
            return mat4.mul(transform, transform, this.transform);
        }
    }

    addChild(node) {
        this.children.push(node);
        node.parent = this;
    }

    removeChild(node) {
        const index = this.children.indexOf(node);
        if (index >= 0) {
            this.children.splice(index, 1);
            node.parent = null;
        }
    }

    traverse(before, after) {
        before && before(this);
        for (let child of this.children) {
            child.traverse(before, after);
        }
        after && after(this);
    }

}

Player.defaults = {
    velocity         : [0, 0, 0],
    gravity         : [0, 0.01, 0],
    c_gravity : 9.81 ,
    canJump : true,
    maxVerticalSpeed: 3,
    maxSpeed         : 3,
    friction         : 0.2,
    acceleration     : 20,
    name: "test",
    translation: [0, 0, 0],
    rotation: [0, 0, 0],
    scale: [1, 1, 1],
    aabb: {
        min: [0, 0, 0],
        max: [0, 0, 0],
    },
};

