import Utils from './Utils.js';

const vec3 = glMatrix.vec3;
const vec4 = glMatrix.vec4;
const mat4 = glMatrix.mat4;
const quat = glMatrix.quat;

export default class Node {

    constructor(options) {
        Utils.init(this, Node.defaults, options);

        this.transform = mat4.create();
        this.updateTransform();

        this.children = [];
        this.parent = null;
    }

    updateTransform() {
        const t = this.transform;
        const degrees = this.rotation.map(x => x * 180 / Math.PI);
        const q = quat.fromEuler(quat.create(), ...degrees);
        const v = vec3.clone(this.translation);
        const s = vec3.clone(this.scale);
        mat4.fromRotationTranslationScale(t, q, v, s);
    }

    moveX(dx){
       this.translation[0] += dx;
    }

    moveY(dx){
       this.translation[1] += dx;
     }
     
     moveZ(dx){
       this.translation[2] += dx;
     }

    
    update(keys,dt) {
        const c = this;

        const forward = vec3.set(vec3.create(),
            -Math.sin(c.rotation[1]), 0, -Math.cos(c.rotation[1]));
        const right = vec3.set(vec3.create(),
            Math.cos(c.rotation[1]), 0, -Math.sin(c.rotation[1]));
        const up = vec3.set(vec3.create(),0,2,0);
        const down = vec3.set(vec3.create(),0,-0.01,0);

        let acc = vec3.create();
        if (keys['KeyA']) {
            vec3.add(acc, acc, forward);
        }
        if (keys['KeyD']) {
            vec3.sub(acc, acc, forward);
        }
        if (keys['KeyW']) {
            vec3.add(acc, acc, right);
        }
        if (keys['KeyS']) {
            vec3.sub(acc, acc, right);
        }
        let vAcc = vec3.create();
        if (c.canJump &&keys['ControlLeft']){
            c.canJump = false;
            vec3.add(acc,acc,up);
        }

        vec3.scaleAndAdd(c.velocity, c.velocity, acc, dt * c.acceleration);
        if (!keys['KeyW'] &&
            !keys['KeyS'] &&
            !keys['KeyD'] &&
            !keys['KeyA'])
        {   
            vec3.scale(c.velocity, c.velocity, 1 - c.friction);
        }

        const len = vec3.len(c.velocity);
        if (len > c.maxSpeed) {
            vec3.scale(c.velocity, c.velocity, c.maxSpeed / len);
        }

    }

    updateRight(keys,dt) {
        const c = this;

        const forward = vec3.set(vec3.create(),
            -Math.sin(c.rotation[1]), 0, -Math.cos(c.rotation[1]));
        const right = vec3.set(vec3.create(),
            Math.cos(c.rotation[1]), 0, -Math.sin(c.rotation[1]));
        const up = vec3.set(vec3.create(),0,2,0);
        const down = vec3.set(vec3.create(),0,-0.01,0);

        let acc = vec3.create();
        if (keys['ArrowLeft']) {
            vec3.add(acc, acc, forward);
        }
        if (keys['ArrowRight']) {
            vec3.sub(acc, acc, forward);
        }
        if (keys['ArrowUp']) {
            vec3.add(acc, acc, right);
        }
        if (keys['ArrowDown']) {
            vec3.sub(acc, acc, right);
        }
        let vAcc = vec3.create();
        if (c.canJump &&keys['ControlRight']){
            if(c.translation[1] <= 2){
                vec3.add(acc,acc,up);
                c.canJump = false;
            }
        }

        vec3.scaleAndAdd(c.velocity, c.velocity, acc, dt * c.acceleration);
        if (!keys['ArrowLeft'] &&
            !keys['ArrowRight'] &&
            !keys['ArrowUp'] &&
            !keys['ArrowDown'])
        {   
            vec3.scale(c.velocity, c.velocity, 1 - c.friction);
        }

        const len = vec3.len(c.velocity);
        if (len > c.maxSpeed) {
            vec3.scale(c.velocity, c.velocity, c.maxSpeed / len);
        }

    }


    getGlobalTransform() {
        if (!this.parent) {
            return mat4.clone(this.transform);
        } else {
            let transform = this.parent.getGlobalTransform();
            return mat4.mul(transform, transform, this.transform);
        }
    }

    addChild(node) {
        this.children.push(node);
        node.parent = this;
    }

    removeChild(node) {
        const index = this.children.indexOf(node);
        if (index >= 0) {
            this.children.splice(index, 1);
            node.parent = null;
        }
    }

    traverse(before, after) {
        before && before(this);
        for (let child of this.children) {
            child.traverse(before, after);
        }
        after && after(this);
    }


}

Node.defaults = {
    shakable: false,
    isShaking: false,
    velocity  : [0, 0, 0],
    gravity  : [0, 0.01, 0],
    portal : [0,0,0],
    c_gravity : 15 ,
    platformable: false,
    canJump : true,
    travelIdx : 0,
    maxVerticalSpeed: 3,
    maxSpeed         : 4,
    friction         : 0.2,
    acceleration     : 20,
    name: "test",
    translation: [0, 0, 0],
    rotation: [0, 0, 0],
    scale: [1, 1, 1],
    aabb: {
        min: [0, 0, 0],
        max: [0, 0, 0],
    },
};
