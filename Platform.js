import Utils from './Utils.js';
import Node from './Node.js';

const mat4 = glMatrix.mat4;
const vec3 = glMatrix.vec3;

export default class Platform extends Node {

    constructor(options) {
        super(options);
        this.updateTransform();

        Utils.init(this, this.constructor.defaults, options);
        console.log(this);

        this.projection = mat4.create();
        this.updateProjection();

    }

    updateProjection() {
        mat4.perspective(this.projection, this.fov, this.aspect, this.near, this.far);
    }


    update(dt) {
        const c = this;
        this.maxVerticalSpeed = 0.3;
        if(this.up){
            this.moveY(this.maxVerticalSpeed);
        }
        else{
            this.moveY(-this.maxVerticalSpeed);
        }
        if(c.translation[2] == c.high[2] || c.translation[2] == c.low[2]){
            this.up =! this.up;
        }
    }

}

Platform.defaults = {
    low: [6,-4,7],
    high  : [6, 4, 6],
    up: true,
    platformable: true,
    maxVerticalSpeed: 3,
    friction         : 0.2,
    acceleration     : 20,
    name: "test",
    velocity         : [0, 0, 0],
    maxSpeed         : 3,
    friction         : 0.2,
    acceleration     : 20
};
