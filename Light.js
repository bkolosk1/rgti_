import Node from './Node.js';

export default class Light extends Node {

    constructor() {
        super();

        Object.assign(this, {
            position         : [2, 2, -1.25],
            ambient          : 0.9,
            diffuse          : 0.8,
            specular         : 1,
            shininess        : 10,
            color            : [0, 1, 0],
            attenuatuion     : [1.0, 0, 0.02]
        });
    }

}