const vertex = `#version 300 es
layout (location = 0) in vec4 aPosition;
layout (location = 1) in vec2 aTexCoord;

uniform mat4 uViewModel;
uniform mat4 uProjection;

out vec2 vTexCoord;

void main() {
    vTexCoord = aTexCoord;
    gl_Position = uProjection * uViewModel * aPosition;
}
`;

const fragment = `#version 300 es
precision mediump float;

uniform mediump sampler2D uTexture;

in vec2 vTexCoord;

out vec4 oColorFog;

void main() {
    vec4 oColor = texture(uTexture, vTexCoord);
     oColorFog = mix(oColor,vec4(0.854,0.478,0.478,0.2),0.4);
}
`;

export default {
    simple: { vertex, fragment }
};
