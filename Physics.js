const vec3 = glMatrix.vec3;
const mat4 = glMatrix.mat4;

export default class Physics {

    constructor(scene,camera,playerLeft,player,platforms,shakables) {
        this.scene = scene;
        this.camera = camera;
        this.playerRight = player;
        this.playerLeft = playerLeft;
        this.platforms = platforms;
        this.shakables = shakables;
    }

    update(dt) {
        this.scene.traverse(node => {
            if (node.velocity) {
                vec3.scaleAndAdd(node.translation, node.translation, node.velocity, dt);
                if(node === this.playerRight){
                    vec3.scaleAndAdd(node.velocity, node.velocity, node.gravity, -node.c_gravity);
                }
                if(node === this.playerLeft){
                    vec3.scaleAndAdd(node.velocity, node.velocity, node.gravity, -node.c_gravity);
                }
                node.updateTransform();
                this.scene.traverse(other => {
                    if (node !== other) {
                        if(node === this.playerRight){
                            this.playerRight.canJump = true; 
                        }
                        if(node === this.playerLeft){
                            this.playerLeft.canJump = true;
                        }
                        this.resolveCollision(node, other);
                    }                   
                });               
            }           
        });
        this.handlePlatforms(this.platforms,dt);
        this.handleShakables(this.camera,this.shakables);        
        //this.handlePlayers(dt);
        //this.handleCamera(dt);
        if(this.playerRight.translation[1] < -10){
            alert('UMRI');
        }
        if(this.playerLeft.translation[1] < -10){
            alert('UMRI');
        }
    }
    
    handleShakables(c,shake){
        let l = shake.length;
        console.log(l);
        for(let i=0;i<l;i++){
            let node = shake[i];
            let colides_first = this.isColliding(node,this.playerLeft);
            let colides_second = this.isColliding(node,this.playerRight);
            if((!colides_first && this.playerLeft.isShaking)){
                c.shake = false;
                this.playerLeft.isShaking = false;
                c.resetCam();
                continue;
            }
            if((!colides_second && this.playerRight.isShaking)){
                c.shake = false;
                this.playerLeft.isShaking = false;
                this.playerRight.isShaking = false;
                c.resetCam();
                continue;
            }
            if(colides_first){
                if(this.playerLeft.isShaking){
                    continue;
                }
                else{
                    this.playerLeft.isShaking = true;
                    c.shake = true;
                }
            }
            else if(colides_second){
                if(this.playerRight.isShaking){
                    continue;
                }
                else{
                    this.playerRight.isShaking = true;
                    c.shake = true;
                }
            }          
        }
        c.updateTransform();
    }
    handleTeleporters(teleporters,dt){
           
    }
    handlePlatforms(platforms,dt){
        let l = platforms.length;
        for(let i=0;i<l;i++){
            let node = platforms[i];
            if(this.isColliding(node,this.playerLeft)||this.isColliding(node,this.playerRight)){
                console.log(node.name);
                node.canJump = false;                              
                node.updateTransform(); 
            }            
        }
        for(let i=0;i<l;i++){
            let node = platforms[i];
            if(!node.canJump){
                vec3.scaleAndAdd(node.translation, node.translation, node.velocity, dt);
                vec3.scaleAndAdd(node.velocity, node.velocity, node.gravity, (node.travelIdx)*dt*node.c_gravity);
            }
        }
                                 
    }

    handlePlayers(dt){
        this.updatePlayer(dt,this.playerLeft);
        this.updatePlayer(dt,this.playerRight);
    }

    handleCamera(dt){
        this.camera.updateTransform();
    }

    updatePlayer(dt,node){
        vec3.scaleAndAdd(node.translation, node.translation, node.velocity, dt);
        vec3.scaleAndAdd(node.velocity, node.velocity, node.gravity, -dt*node.c_gravity);
        node.updateTransform();
        this.scene.traverse(other => {
            if (node !== other) {
                    if(node !== this.playerRight){
                        this.playerRight.canJump = true; 
                    }
                    if(node === this.playerLeft){
                        this.playerLeft.canJump = true;
                    }
                this.resolveCollision(node, other);
            }                   
        });            
 
    }

    isColliding(a,b){
        const ta = a.getGlobalTransform();
        const tb = b.getGlobalTransform();

        const posa = mat4.getTranslation(vec3.create(), ta);
        const posb = mat4.getTranslation(vec3.create(), tb);

        const mina = vec3.add(vec3.create(), posa, a.aabb.min);
        const maxa = vec3.add(vec3.create(), posa, a.aabb.max);
        const minb = vec3.add(vec3.create(), posb, b.aabb.min);
        const maxb = vec3.add(vec3.create(), posb, b.aabb.max);

        // Check if there is collision.
        const isColliding = this.aabbIntersection({
            min: mina,
            max: maxa
        }, {
            min: minb,
            max: maxb
        });
        return isColliding;
    }


    intervalIntersection(min1, max1, min2, max2) {
        return !(min1 > max2 || min2 > max1);
    }

    aabbIntersection(aabb1, aabb2) {
        return this.intervalIntersection(aabb1.min[0], aabb1.max[0], aabb2.min[0], aabb2.max[0])
            && this.intervalIntersection(aabb1.min[1], aabb1.max[1], aabb2.min[1], aabb2.max[1])
            && this.intervalIntersection(aabb1.min[2], aabb1.max[2], aabb2.min[2], aabb2.max[2]);
    }

    resolveCollision(a, b) {
        // Update bounding boxes with global translation.
        const ta = a.getGlobalTransform();
        const tb = b.getGlobalTransform();

        const posa = mat4.getTranslation(vec3.create(), ta);
        const posb = mat4.getTranslation(vec3.create(), tb);

        const mina = vec3.add(vec3.create(), posa, a.aabb.min);
        const maxa = vec3.add(vec3.create(), posa, a.aabb.max);
        const minb = vec3.add(vec3.create(), posb, b.aabb.min);
        const maxb = vec3.add(vec3.create(), posb, b.aabb.max);

        // Check if there is collision.
        const isColliding = this.aabbIntersection({
            min: mina,
            max: maxa
        }, {
            min: minb,
            max: maxb
        });

        if (!isColliding) {
            return;
        }

        // Move node A minimally to avoid collision.
        const diffa = vec3.sub(vec3.create(), maxb, mina);
        const diffb = vec3.sub(vec3.create(), maxa, minb);

        let minDiff = Infinity;
        let minDirection = [0, 0, 0];
        if (diffa[0] >= 0 && diffa[0] < minDiff) {
            minDiff = diffa[0];
            minDirection = [minDiff, 0, 0];
        }
        if (diffa[1] >= 0 && diffa[1] < minDiff) {
            minDiff = diffa[1];
            minDirection = [0, minDiff, 0];
        }
        if (diffa[2] >= 0 && diffa[2] < minDiff) {
            minDiff = diffa[2];
            minDirection = [0, 0, minDiff];
        }
        if (diffb[0] >= 0 && diffb[0] < minDiff) {
            minDiff = diffb[0];
            minDirection = [-minDiff, 0, 0];
        }
        if (diffb[1] >= 0 && diffb[1] < minDiff) {
            minDiff = diffb[1];
            minDirection = [0, -minDiff, 0];
        }
        if (diffb[2] >= 0 && diffb[2] < minDiff) {
            minDiff = diffb[2];
            minDirection = [0, 0, -minDiff];
        }

        vec3.add(a.translation, a.translation, minDirection);
        a.updateTransform();
    }

}
