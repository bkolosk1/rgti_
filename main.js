import Application from './Application.js';
import Renderer from './Renderer.js';
import Physics from './Physics.js';
import Camera from './Camera.js';
import SceneLoader from './SceneLoader.js';
import SceneBuilder from './SceneBuilder.js';
import Light from './Light.js';

class App extends Application {

    start() {
        const gl = this.gl;

        this.renderer = new Renderer(gl);
        this.time = Date.now();
        this.startTime = this.time;
        this.aspect = 1;

        this.keydownHandler = this.keydownHandler.bind(this);
        this.keyupHandler = this.keyupHandler.bind(this);
        this.pointerlockchangeHandler = this.pointerlockchangeHandler.bind(this);

        document.addEventListener('pointerlockchange', this.pointerlockchangeHandler);
        document.addEventListener('keydown', this.keydownHandler);
        document.addEventListener('keyup', this.keyupHandler);
        
        
        this.keys = {};

        this.load('scene.json');
    }

    async load(uri) {
        const scene = await new SceneLoader().loadScene('scene.json');
        const builder = new SceneBuilder(scene);
        this.scene = builder.build();

        //Import light
        this.light = new Light();
        this.scene.addNode(this.light);

        // Find first camera.
        this.camera = null;
        this.player = null;
        this.playerLeft = null;
        this.platform = [];
        this.shakables = [];
        this.scene.traverse(node => {
            if (node instanceof Camera) {
                this.camera = node;
            }
            if (node.name === "player"){
                this.player = node;
            }
            if (node.platformable){
                this.platform.push(node);
            }
            if(node.name === "playerLeft"){
                this.playerLeft = node;
            }
            if(node.shakable){
                this.shakables.push(node);
            }
        });

        this.physics = new Physics(this.scene,this.camera,this.playerLeft,this.player,this.platform,this.shakables);
        this.camera.aspect = this.aspect;
        this.camera.updateProjection();
        this.renderer.prepare(this.scene);
    }

    enableCamera() {
        this.canvas.requestPointerLock();
    }

    

    pointerlockchangeHandler() {
        if (!this.camera) {
            return;
        }

        if (document.pointerLockElement === this.canvas) {
            this.camera.enable();
        } else {
            this.camera.disable();
        }
    }

    update() {
        const t = this.time = Date.now();
        const dt = (this.time - this.startTime) * 0.001;
        this.startTime = this.time;
        if (this.camera) {
            this.camera.update(dt,this.player,this.playerLeft);
        }
        if(this.player){    
            this.player.update(this.keys,dt);            
        }
        if(this.playerLeft){
            this.playerLeft.updateRight(this.keys,dt);
        }
        if (this.physics) {
            this.physics.update(dt);
        }
    }

    render() {
        if (this.scene) {
            this.renderer.render(this.scene, this.camera);
        }
    }

    resize() {
        const w = this.canvas.clientWidth;
        const h = this.canvas.clientHeight;
        this.aspect = w / h;
        if (this.camera) {
            this.camera.aspect = this.aspect;
            this.camera.updateProjection();
        }
    }
    
    keydownHandler(e) {
        this.keys[e.code] = true;
    }

    keyupHandler(e) {
        this.keys[e.code] = false;
    }

}

document.addEventListener('DOMContentLoaded', () => {
    const canvas = document.querySelector('canvas');
    const app = new App(canvas);
    gui.add(app, 'enableCamera');
});
