import Utils from './Utils.js';
import Node from './Node.js';

const mat4 = glMatrix.mat4;
const vec3 = glMatrix.vec3;

export default class Camera extends Node {

    constructor(options) {
        super(options);
        Utils.init(this, this.constructor.defaults, options);
        this.canvasWidth = 0;
        this.canvasHeight = 0;
        this.projection = mat4.create();
        this.startedShake = true;
        this.saved = vec3.create();
        this.updateProjection();
        this.shakeAmount = 0;
        this.mousemoveHandler = this.mousemoveHandler.bind(this);
        this.keydownHandler = this.keydownHandler.bind(this);
        this.keyupHandler = this.keyupHandler.bind(this);
        this.keys = {};
    }

    updateProjection() {
        mat4.perspective(this.projection, this.fov, this.aspect, this.near, this.far);
    }

    resetCam(){
        console.log(this.rotation);
        vec3.copy(this.rotation,this.saved);
        this.startedShake = true;
    }
    update(dt,p1,p2) {
        const c = this;
        const up = vec3.set(vec3.create(),0,1,0);
        if(p1.translation[0]-c.translation[0] > 5 || p2.translation[0]-c.translation[0] > 5){
            c.moveX(5);
        }
        if(-p1.translation[0]+c.translation[0] > 5 || -p2.translation[0]+c.translation[0] > 5){
            c.moveX(-5);
        }
        if(this.shake){
            if(this.startedShake){
                vec3.copy(c.saved,c.rotation);
                this.startedShake = false;
            }
            c.rotation[0] = Math.sin(this.shakeAmount);
            this.shakeAmount += 0.1;
        }
    }

    enable() {
        document.addEventListener('mousemove', this.mousemoveHandler);
        document.addEventListener('keydown', this.keydownHandler);
        document.addEventListener('keyup', this.keyupHandler);
    }

    disable() {
        document.removeEventListener('mousemove', this.mousemoveHandler);
        document.removeEventListener('keydown', this.keydownHandler);
        document.removeEventListener('keyup', this.keyupHandler);

        for (let key in this.keys) {
            this.keys[key] = false;
        }
    }

    mousemoveHandler(e) {
        const dx = e.movementX;
        const dy = e.movementY;
        const c = this;

        c.rotation[0] -= dy * c.mouseSensitivity;
        c.rotation[1] -= dx * c.mouseSensitivity;

        const pi = Math.PI;
        const twopi = pi * 2;
        const halfpi = pi / 2;

        if (c.rotation[0] > halfpi) {
            c.rotation[0] = halfpi;
        }
        if (c.rotation[0] < -halfpi) {
            c.rotation[0] = -halfpi;
        }

        c.rotation[1] = ((c.rotation[1] % twopi) + twopi) % twopi;
    }

    keydownHandler(e) {
        this.keys[e.code] = true;
    }

    keyupHandler(e) {
        this.keys[e.code] = false;
    }

}

Camera.defaults = {
    shake: false,
    aspect           : 1,
    fov              : 1.5,
    near             : 0.01,
    far              : 100,
    velocity         : [0, 0, 0],
    mouseSensitivity : 0.002,
    maxSpeed         : 3,
    friction         : 0.2,
    acceleration     : 20
};
